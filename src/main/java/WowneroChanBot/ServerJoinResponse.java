package WowneroChanBot;

import net.dv8tion.jda.api.events.guild.GuildJoinEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class ServerJoinResponse extends ListenerAdapter {
    public void onGuildJoin(GuildJoinEvent guildJoinEvent) {
        guildJoinEvent.getGuild().getDefaultChannel().sendMessageEmbeds(new Responses().serverJoinReply()).queue();
    }
}
