package WowneroChanBot;

import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class MemberJoinResponse extends ListenerAdapter {
    public void onGuildMemberJoin(GuildMemberJoinEvent guildMemberJoinEvent) {
        guildMemberJoinEvent.getGuild().getDefaultChannel().sendMessage(new Responses().memberJoinReply(guildMemberJoinEvent.getUser().getName())).queue();
    }
}
