package WowneroChanBot;

import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.util.Arrays;

import static WowneroChanBot.Converter.*;
import static WowneroChanBot.WowneroChanBot.BOT_NAME;
import static WowneroChanBot.WowneroChanBot.jda;

public class Commands extends ListenerAdapter {

    private static final String COMMAND_PREFIX = "/";

    private static final String ABOUT_COMMAND = "/about";
    private static final String GETTING_STARTED_COMMAND = "/start";
    private static final String ELI5_COMMAND = "/eli5";

    private static final String HELP_COMMAND = "/help";
    private static final String COMMANDS_COMMAND = "/commands";

    private static final String GET_WOW_COMMAND = "/getwow";
    private static final String MINING_COMMAND = "/minewow";

    private static final String MEMES_COMMAND = "/meme";

    private static final String CHOOSE_WOWLET_COMMAND = "/choosewowlet";
    private static final String CLI_WOWLET_COMMAND = "/cli";
    private static final String GUI_WOWLET_COMMAND = "/desktop";
    private static final String WEB_WOWLET_COMMAND = "/web";
    private static final String WOWNERUJO_COMMAND = "/wownerujo";
    private static final String PAPER_WOWLET_COMMAND = "/paper";
    private static final String ALL_WOWLETS_COMMAND = "/allwowlets";

    private static final String HI_COMMAND = "/hi";
    private static final String RICH_LIST_COMMAND = "/richlist";

    private static final String WOW_PRICE_COMMAND = "/price";
    private static final String WOW_TO_USD_COMMAND = "/wowusd";
    private static final String USD_TO_WOW_COMMAND = "/usdwow";
    private static final String WOW_TO_BTC_COMMAND = "/wowbtc";
    private static final String BTC_TO_WOW_COMMAND = "/btcwow";
    private static final String XMR_TO_WOW_COMMAND = "/xmrwow";
    private static final String WOW_TO_XMR_COMMAND = "wowxmr";

    private static final String DONATE_COMMAND = "/donate";

    public void onMessageReceived(MessageReceivedEvent msgReceivedEvent) {
        String[] msgArray = msgReceivedEvent.getMessage().getContentRaw().split("\\s+");
        String authorName = msgReceivedEvent.getAuthor().getName();
        Responses responses = new Responses();

        MessageChannel messageChannel = getMessageChannel(msgReceivedEvent);

        if(msgArray[0].startsWith(COMMAND_PREFIX)) {
            switch (msgArray[0]) {
                case HI_COMMAND -> messageChannel.sendMessage(responses.hiReply(authorName)).queue();
                case ABOUT_COMMAND -> messageChannel.sendMessageEmbeds(responses.aboutReply()).queue();
                case GETTING_STARTED_COMMAND -> messageChannel.sendMessageEmbeds(responses.gettingStartedReply()).queue();
                case ELI5_COMMAND -> messageChannel.sendMessageEmbeds(responses.eliFiveReply()).queue();
                case COMMANDS_COMMAND, HELP_COMMAND -> messageChannel.sendMessageEmbeds(responses.commandsReply()).queue();
                case MINING_COMMAND -> messageChannel.sendMessageEmbeds(responses.miningReply()).queue();
                case GET_WOW_COMMAND -> messageChannel.sendMessageEmbeds(responses.getWowReply()).queue();
                case MEMES_COMMAND -> messageChannel.sendMessageEmbeds(responses.memeReply()).queue();
                case RICH_LIST_COMMAND -> messageChannel.sendMessageEmbeds(responses.richListReply()).queue();
                case DONATE_COMMAND -> messageChannel.sendMessageEmbeds(responses.donateReply()).queue();
                case WOW_PRICE_COMMAND -> messageChannel.sendMessageEmbeds(responses.getPriceReply()).queue();
                case WOW_TO_USD_COMMAND -> messageChannel.sendMessageEmbeds(responses.conversionReply(WOW_TO_USD, (checkForSecondWord(msgArray)) ? msgArray[1] : null)).queue();
                case USD_TO_WOW_COMMAND -> messageChannel.sendMessageEmbeds(responses.conversionReply(USD_TO_WOW, (checkForSecondWord(msgArray)) ? msgArray[1] : null)).queue();
                case WOW_TO_BTC_COMMAND -> messageChannel.sendMessageEmbeds(responses.conversionReply(WOW_TO_BTC, (checkForSecondWord(msgArray)) ? msgArray[1] : null)).queue();
                case BTC_TO_WOW_COMMAND -> messageChannel.sendMessageEmbeds(responses.conversionReply(BTC_TO_WOW, (checkForSecondWord(msgArray)) ? msgArray[1] : null)).queue();
                case XMR_TO_WOW_COMMAND -> messageChannel.sendMessageEmbeds(responses.conversionReply(XMR_TO_WOW, (checkForSecondWord(msgArray)) ? msgArray[1] : null)).queue();
                case WOW_TO_XMR_COMMAND -> messageChannel.sendMessageEmbeds(responses.conversionReply(WOW_TO_XMR, (checkForSecondWord(msgArray)) ? msgArray[1] : null)).queue();
                case CHOOSE_WOWLET_COMMAND -> messageChannel.sendMessageEmbeds(responses.walletTypesReply()).queue();
                case CLI_WOWLET_COMMAND -> messageChannel.sendMessageEmbeds(responses.cliReply()).queue();
                case GUI_WOWLET_COMMAND -> messageChannel.sendMessageEmbeds(responses.guiReply()).queue();
                case WEB_WOWLET_COMMAND -> messageChannel.sendMessageEmbeds(responses.webReply()).queue();
                case WOWNERUJO_COMMAND -> messageChannel.sendMessageEmbeds(responses.wownerujoReply()).queue();
                case PAPER_WOWLET_COMMAND -> messageChannel.sendMessageEmbeds(responses.paperReply()).queue();
                case ALL_WOWLETS_COMMAND -> messageChannel.sendMessageEmbeds(responses.allWowletsReply()).queue();
            }
        } else {
            boolean hi = (msgArray[0].equals("hi") && msgArray.length == 1)
                    || (msgArray[0].equals("hey") && msgArray.length == 1) || (msgArray[0].equals("hello") && msgArray.length == 1)
                    || checkMessageForStringCombo(msgArray, new String[]{"Hi", BOT_NAME}, msgReceivedEvent.getAuthor().getName())
                    || checkMessageForStringCombo(msgArray, new String[]{"Hey", BOT_NAME}, msgReceivedEvent.getAuthor().getName())
                    || checkMessageForStringCombo(msgArray, new String[]{"Hello", BOT_NAME}, msgReceivedEvent.getAuthor().getName());
            boolean uwu = checkMessageForStringCombo(msgArray, new String[]{"uwu"}, msgReceivedEvent.getAuthor().getName());
            boolean owo = checkMessageForStringCombo(msgArray, new String[]{"owo"}, msgReceivedEvent.getAuthor().getName());
            boolean heart = (msgArray[0].equals("<3") && msgArray.length == 1 && !authorName.equals(BOT_NAME)) ||
                    checkMessageForStringCombo(msgArray, new String[]{BOT_NAME, "<3"}, msgReceivedEvent.getAuthor().getName()) ||
                    checkMessageForStringCombo(msgArray, new String[]{"I", "love", BOT_NAME}, msgReceivedEvent.getAuthor().getName());

            if(hi || uwu || owo || heart) {
                msgReceivedEvent.getChannel().sendTyping().queue();
            }

            if (hi) {
                messageChannel.sendMessage(responses.hiReply(authorName)).queue();
            }
            else if (uwu) {
                messageChannel.sendMessage(responses.uwuReply()).queue();
            }
            else if (owo) {
                messageChannel.sendMessage(responses.owoReply()).queue();
            }
            else if (heart) {
                messageChannel.sendMessage(responses.heartReply()).queue();
            }
        }
    }

    private static MessageChannel getMessageChannel(MessageReceivedEvent msgReceivedEvent) {
        try {
            return msgReceivedEvent.getPrivateChannel();
        } catch (IllegalStateException notPrivateChannelException) {
            return msgReceivedEvent.getChannel();
        }
    }

    private static boolean checkForSecondWord(String[] msgArray) {
        try {
            if(msgArray[1].isBlank() || msgArray[1].isEmpty()) {
                return false;
            }
        } catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException) {
            return false;
        }
        return true;
    }

    private static boolean checkMessageForStringCombo(String[] messageArray, String[] searchWords, String authorName) {
        boolean allWordsFound = false;

        for (String searchWord : searchWords) {
            if ((Arrays.stream(messageArray).anyMatch(searchWord::equalsIgnoreCase)) &&
                    !authorName.equalsIgnoreCase(BOT_NAME)) {
                allWordsFound = true;
            } else {
                return false;
            }
        }
        return allWordsFound;
    }
}
