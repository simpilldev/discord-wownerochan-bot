package WowneroChanBot;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.requests.GatewayIntent;
import javax.security.auth.login.LoginException;

public class WowneroChanBot {

    public static JDA jda;

    public static final String BOT_NAME = "WowneroChan";
    public static final String BOT_API_KEY = "";
    public static final String CURRENT_GAME = "Boating Simulator";

    public static final String DEV_WOW_ADDRESS = "WW3gsJjJHgTXrMgYDkQHW1DAcWFxxqUmvAYCnBda3mGLiDesyeJX9PhS1URRNihQny727EzbJxqCG6XL7tV3awuR29QTRqDcB";
    public static final String DEV_XMR_ADDRESS = "88KBTppCLxZb2qS4jqfVfXN3kqioyqtWkjf9RRY9LSW3GTwXxoCX3x7UxYnAoZi3XjHDBfVBBFPZ28fppHPPjSg1MJY6eg6";

    public static void main (String[] args) throws LoginException {
        jda = JDABuilder.createDefault(BOT_API_KEY)
                .setActivity(Activity.playing(CURRENT_GAME))
                .setStatus(OnlineStatus.ONLINE)
                .enableIntents(GatewayIntent.GUILD_MEMBERS)
                .addEventListeners(new Commands(), new MemberJoinResponse(), new ServerJoinResponse())
                .build();
    }
}

