package WowneroChanBot;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;

import javax.annotation.Nullable;

import java.util.Collection;
import java.util.List;

import static WowneroChanBot.Converter.*;
import static WowneroChanBot.Memes.*;
import static WowneroChanBot.WowneroChanBot.*;

public class Responses {

    private static final String WOWNEROCHAN_IMG_URL = "https://i.redd.it/i2ovdj34ze481.png";
    private static final String WOWNERO_IMG_URL = "https://gitlab.com/simpilldev/discord-wownerochan-bot/-/raw/master/img/wownero-spin.gif";
    private static final int WOWNERO_PINK = 0xEA12FF;
    private static final String EMBED_FOOTER = BOT_NAME + " \uD83E\uDD0D";
    private static final String COINGECKO_LINK = "https://www.coingecko.com/en/coins/wownero";

    public static final String WOW_TO_USD_MISSING_AMOUNT_REPLY = "Sorry Anon, I need an amount of WOW to convert :(\n\nTry it like this:\n`/wowusd 42069.420`";
    public static final String USD_TO_WOW_MISSING_AMOUNT_REPLY = "Sorry Anon, I need an amount of USD to convert :(\n\nTry it like this:\n`/usdwow 420.69`";
    public static final String WOW_TO_BTC_MISSING_AMOUNT_REPLY = "Sorry Anon, I need an amount of WOW to convert :(\n\nTry it like this:\n`/wowbtc 420.69`";
    public static final String BTC_TO_WOW_MISSING_AMOUNT_REPLY = "Sorry Anon, I need an amount of BTC to convert :(\n\nTry it like this:\n`/btcwow 0.42069`";
    public static final String WOW_TO_XMR_MISSING_AMOUNT_REPLY = "Sorry Anon, I need an amount of XMR to convert :(\n\nTry it like this:\n`/wowxmr 42069.42069`";
    public static final String XMR_TO_WOW_MISSING_AMOUNT_REPLY = "Sorry Anon, I need an amount of XMR to convert :(\n\nTry it like this:\n`/xmrwow 4.2069`";


    private static Message createMessage(String msg) {
        return new MessageBuilder()
                .setContent(msg)
                .build();
    }
    private static MessageEmbed createMessageEmbed(@Nullable String embedTitle, @Nullable String embedTitleUrl,
                                                   @Nullable String description, @Nullable String imgUrl, int color) {
        EmbedBuilder embedBuilder = new EmbedBuilder();

        if (embedTitle != null && embedTitleUrl == null) {
            embedBuilder.setTitle(embedTitle);
        }
        if (embedTitle != null && embedTitleUrl != null) {
            embedBuilder.setTitle(embedTitle, embedTitleUrl);
        }
        if(description != null) {
            embedBuilder.setDescription(description);
        }
        if (imgUrl != null) {
            embedBuilder.setImage(imgUrl);
        }
        embedBuilder.setFooter(EMBED_FOOTER, WOWNEROCHAN_IMG_URL);
        embedBuilder.setColor(color);


        return embedBuilder.build();
    }

    public MessageEmbed serverJoinReply() {
        String embedTitle = BOT_NAME + ": Putting the 'fun' in fungibility";
        String description = "Hi Anons, it's " + BOT_NAME + ":3\n\nFor a full list of my commands, type:\n`/commands`";

        return createMessageEmbed(embedTitle, WOWNEROCHAN_IMG_URL, description, WOWNEROCHAN_IMG_URL, WOWNERO_PINK);
    }
    public Message memberJoinReply(String memberName) {
        final String welcomeMsg = "Welcome " + memberName + " :3 I hope you have your boating licence ;)";
        return createMessage(welcomeMsg);
    }

    //Miscellaneous
    public Message hiReply(String authorName) {
        final String hiResponse = "Hi " + authorName + " :3";
        return createMessage(hiResponse);
    }
    public Message uwuReply() {
        final String uwuResponse = "uwu :3";
        return createMessage(uwuResponse);
    }
    public Message owoReply() {
        final String uwuResponse = "owo :3";
        return createMessage(uwuResponse);
    }
    public Message heartReply() {
        final String heartResponse = "<3";
        return createMessage(heartResponse);
    }
    public MessageEmbed richListReply() {
        String embedTitle = "Wownero Rich List";
        String richList = "No 1.\n??? owns ??? WOW\n\nNo 2.\n??? owns ??? WOW\n\nNo 3.\n??? owns ??? WOW\n\nNo 4.\n??? owns ??? WOW\n\nNo 5.\n??? owns ??? WOW";

        return createMessageEmbed(embedTitle, null, richList, null, WOWNERO_PINK);
    }
    public MessageEmbed memeReply() {
        String[] memeData = new Memes().getRandomMeme();

        String embedTitle = "Meme from " + memeData[authorName];
        String embedTitleUrl = memeData[authorUrl];
        String description = "Donate some WOW to [" + memeData[authorName] + "](" + memeData[authorUrl] + "):\n" + memeData[authorAddress];

        return createMessageEmbed(embedTitle, embedTitleUrl, description, memeData[url].equalsIgnoreCase("null") ? null : memeData[url], WOWNERO_PINK);
    }

    public MessageEmbed commandsReply() {
        final String embedTitle = BOT_NAME + " Commands";
        final String gettingStartedCommandsDescription = """
            **Getting Started**
           `/about` - Display a brief description of Wownero.
           `/eli5` - Display an ELI5 description of Wownero.
           `/start` - Display a webpage link that will help you get started with Wownero.""";

        final String wowletsCommandsDescription = """
           **Wowlets**
           `/choosewowlet` - Guide on the different wowlets.
           `/cli` - Display info about the CLI wowlet.
           `/desktop` - Display info about the desktop wowlet.
           `/web` - Display info about the web wowlet.
           `/wownerujo` - Display info about the Wownerujo wowlet.
           `/paper` - Display info about paper wowlets.
           `/allwowlets` - Display the info for each above wowlets.""";

        final String gettingWowCommandsDescription = """
           **Getting WOW**
           `/getwow` - Guide to getting some WOW.
           `/mining` - Guide to mining some WOW.""";

        final String miscellaneousCommandsDescription = """
           **Miscellaneous**
           `/hi` - Say hi to WowneroChan.
           `/richlist` - Display the top 5 WOW addresses and their balances.""";


        final String conversionCommandsDescription = """
            **WOW Conversions**
            `/usdwow` - USD -> WOW
            `/wowusd` - WOW -> USD
            `/btcwow` - BTC -> WOW
            `/wowbtc` - WOW -> BTC
            `/xmrwow` - XMR -> WOW
            `/wowxmr` - WOW -> XMR""";

        final String donationsCommandDescription = """
                **Donations**
                `/donate` - Display my WOW & XMR donation addresses. Such appreciation, many stonks.""";

        final String description = gettingStartedCommandsDescription + "\n\n" + wowletsCommandsDescription
                + "\n\n" + gettingWowCommandsDescription + "\n\n" + miscellaneousCommandsDescription
                + "\n\n" + conversionCommandsDescription + "\n\n" + donationsCommandDescription;

        return createMessageEmbed(embedTitle, null, description, null, WOWNERO_PINK);
    }

    //Getting Started Responses
    public MessageEmbed aboutReply() {
        final String embedTitle = "About Wownero";
        final String embedTitleUrl = "https://wownero.org/";
        final String embedMsg = "Wownero is the lovechild of Dogecoin and Monero i.e. many rings, such privacy";
        return createMessageEmbed(embedTitle, embedTitleUrl, embedMsg, WOWNERO_IMG_URL, WOWNERO_PINK);
    }
    public MessageEmbed eliFiveReply() {
        String embedTitle = "Wownero ELI5";
        String description = """
                Everyone stores their funds using a software application called a Wownero wallet (wowlet). Each wowlet has its own receiving addresses. You tell other degens one of your addresses so that they can send you funds.

                When another degen sends you funds, you can’t tell who sent it to you (unless they want you to know). Similarly, when you send funds, the remtard on the receiving end won’t know it was you that sent it, unless you tell them it was you.

                Wownero’s privacy is important to prevent others from knowing how rich you are and to prevent them from spying on how you spend your money. It also keeps your *business* transactions completely confidential and Indian scammers from stealing hard-earned WOW.

                Wownero has no central point of authority. When you send funds to someone, a worldwide network of PCs (owned by degen remtards) will come to an agreement among themselves that ownership of the funds has passed from one anonymous person to another. This means Wownero cannot be shut down by any one country or authority.
                
                To get started with Wownero, all you need to do is download a wowlet. Then buy some Wownero from an exchange and withdraw it to your own wowlet. Congrats, you are now the owner of some WOW.\s""";

        return createMessageEmbed(embedTitle, null, description, WOWNERO_IMG_URL, WOWNERO_PINK);
    }
    public MessageEmbed gettingStartedReply() {
        String embedTitle = "Wownero: Getting Started";
        String embedTitleUrl = "https://such.money/";

        return createMessageEmbed(embedTitle, embedTitleUrl, null, WOWNERO_IMG_URL, WOWNERO_PINK);
    }

    //Wowlet Replies
    public MessageEmbed walletTypesReply() {
        final String embedTitle = "Wowlet Types";
        final String embedTitleUrl = "https://wownero.org/#wallets";
        final String description = """
                Software Wallet: A piece of software used to send, receive & manage WOW. Web wowlets, desktop wowlets, mobile wowlets, GUI & CLI wowlets are all examples of software wowlets.
                
                Hardware Wowlet: A physical device that encrypts and stores your private keys completely offline. The device must be connected to a computer to access the funds.
                
                Paper Wowlet: A piece of paper that contains your public and private keys, usually in plain text or as a QR code.
                
                Hot Wowlet: A wowlet that is connected to the internet e.g. web, desktop & mobile wowlets.
                
                Cold Wowlet: A wowlet that has no connection to the internet e.g. hardware or paper wowlets.
                """;

        return createMessageEmbed(embedTitle, embedTitleUrl, description, null, WOWNERO_PINK);
    }
    public MessageEmbed cliReply() {
        final String embedTitle = "CLI Wowlet";
        final String embedTitleUrl = "https://git.wownero.com/wownero/wownero/releases";
        final String description = "The CLI (command line interface) wowlet is a wowlet that allows you to send, receive and manage your WOW directly from the command line.";
        final String imgUrl = "https://gitlab.com/simpilldev/discord-wownerochan-bot/-/raw/master/img/cli-wowlet.png";

        return createMessageEmbed(embedTitle, embedTitleUrl, description, imgUrl, WOWNERO_PINK);
    }
    public MessageEmbed guiReply() {
        final String embedTitle = "Desktop Wowlet";
        final String embedTitleUrl = "https://git.wownero.com/wowlet/wowlet/releases";
        final String description = "A standard desktop wowlet for sending, receiving and managing your WOW.";
        final String imgUrl = "https://i.imgur.com/hb0ypVr.png";

        return createMessageEmbed(embedTitle, embedTitleUrl, description, imgUrl, WOWNERO_PINK);
    }
    public MessageEmbed webReply() {
        final String embedTitle = "Web Wowlet";
        final String embedTitleUrl = "https://wowstash.app/";
        final String description = "Wowstash is a web-based wowlet that allows you to use WOW regardless of what device you use.";
        final String imgUrl = "https://gitlab.com/simpilldev/discord-wownerochan-bot/-/raw/master/img/wow-stash.png";

        return createMessageEmbed(embedTitle, embedTitleUrl, description, imgUrl, WOWNERO_PINK);
    }
    public MessageEmbed wownerujoReply() {
        final String embedTitle = "Wownerujo Wowlet";
        final String embedTitleUrl = "https://forum.wownero.com/t/how-to-install-wonerujo-android-wallet/358";
        final String description = "Wownerujo is an Android only wowlet based on Monerujo.";
        final String imgUrl = "https://play-lh.googleusercontent.com/2jR17JdJdbBr8vULqdOcWXQLv8AaSky1CQvjwm_N-w2sD4h6RL894-O2Anv7CDjy-Qg=s180";

        return createMessageEmbed(embedTitle, embedTitleUrl, description, imgUrl, WOWNERO_PINK);
    }
    public MessageEmbed paperReply() {
        final String embedTitle = "Paper Wowlet";
        final String embedTitleUrl = "https://gitlab.com/simpilldev/discord-monerochan-bot/-/blob/master/guides/paperwalletguide.md";
        final String description = """
        A paper wowlet is a piece of paper that contains your public and private keys, usually in plain text or as a QR code.
        
        Here's a guide I wrote on creating paper wallets for Monero (same applies for Wownero):
        
        [How to Create a Paper XMR Wallet using Tails](https://gitlab.com/simpilldev/discord-monerochan-bot/-/blob/master/guides/paperwalletguide.md)
        
        [Wownero Paper Wallet Generator](https://git.wownero.com/wownero/wownero-wallet-generator/src/branch/master/wownero-wallet-generator.html)""";

        return createMessageEmbed(embedTitle, embedTitleUrl, description, null, WOWNERO_PINK);
    }
    public List<MessageEmbed> allWowletsReply() {
        return List.of(cliReply(), guiReply(), webReply(), wownerujoReply(), paperReply());
    }

    public MessageEmbed miningReply() {
        String embedTitle = "How to Mine WOW";
        String embedTitleUrl = "https://forum.wownero.com/t/how-to-solo-mine/551";
        String description = """
                Download the [WOW CLI](https://git.wownero.com/wownero/wownero/releases)
                
                Extract the downloaded file.
                
                Run the daemon in a terminal with the following command:
                `./wownerod --start-mining YOUR-ADDRESS-HERE --spendkey YOUR-SECRET-SPENDKEY-HERE`.
                
                Replace the `./wownerod` with `wownerod.exe` if you are a Windows remtard.""";
        String imgUrl = "https://gitlab.com/simpilldev/discord-wownerochan-bot/-/raw/master/img/cpu-mining.png";
        return createMessageEmbed(embedTitle, embedTitleUrl, description, imgUrl, WOWNERO_PINK);
    }

    public MessageEmbed donateReply() {
        final String embedTitle = "Donate to the developer :)";
        final String embedTitleUrl = "https://wownero.org/";
        final String imgUrl = "https://www.pinclipart.com/picdir/big/55-552238_9168049-apu-pepe-thumbs-up-clipart.png";
        final String embedMsg = "Are you enjoying the WowneroChan Bot? Please consider donating some WOW or XMR to the degen remtard *ahem* developer who made it.\n\nWOW Address:\n" + DEV_WOW_ADDRESS + "\n\nXMR Address:\n" + DEV_XMR_ADDRESS;
        return createMessageEmbed(embedTitle, embedTitleUrl, embedMsg, imgUrl, WOWNERO_PINK);
    }

    //Conversion Responses
    public MessageEmbed getPriceReply() {
        Converter converter = new Converter();

        String embedTitle = "WOW Price";
        String description = "1 WOW = " + converter.getWowPriceInUsdAsString();

        return createMessageEmbed(embedTitle, COINGECKO_LINK, description, null, WOWNERO_PINK);
    }
    public MessageEmbed conversionReply(int conversionNumber, @Nullable String amount) {
        String embedTitle;
        String description;
        switch (conversionNumber) {
            case WOW_TO_USD -> {
                embedTitle = "WOW to USD Conversion";
                try {
                    description = amount + " WOW is currently worth around " + new Converter().convertWowToUsd(amount);
                } catch (NumberFormatException | NullPointerException amountWrongOrMissingException) {
                    description = WOW_TO_USD_MISSING_AMOUNT_REPLY;
                }
            }
            case USD_TO_WOW -> {
                embedTitle = "USD to WOW Conversion";
                try {
                    description = amount + " USD is currently worth around " + new Converter().convertUsdToWow(amount);
                } catch (NumberFormatException | NullPointerException amountWrongOrMissingException) {
                    description = USD_TO_WOW_MISSING_AMOUNT_REPLY;
                }
            }
            case WOW_TO_BTC -> {
                embedTitle = "WOW to BTC Conversion";
                try {
                    description = amount + " WOW is currently worth around " + new Converter().convertWowToBtc(amount);
                } catch (NumberFormatException | NullPointerException amountWrongOrMissingException) {
                    description = WOW_TO_BTC_MISSING_AMOUNT_REPLY;
                }
            }
            case BTC_TO_WOW -> {
                embedTitle = "WOW to BTC Conversion";
                try {
                    description = amount + " BTC is currently worth around " + new Converter().convertBtcToWow(amount);
                } catch (NumberFormatException | NullPointerException amountWrongOrMissingException) {
                    description = BTC_TO_WOW_MISSING_AMOUNT_REPLY;
                }
            }
            case WOW_TO_XMR -> {
                embedTitle = "WOW to XMR Conversion";
                try {
                    description = amount + " WOW is currently worth around " + new Converter().convertWowToXmr(amount);
                } catch (NumberFormatException | NullPointerException amountWrongOrMissingException) {
                    description = WOW_TO_XMR_MISSING_AMOUNT_REPLY;
                }
            }
            case XMR_TO_WOW -> {
                embedTitle = "XMR to WOW Conversion";
                try {
                    description = amount + " XMR is currently worth around " + new Converter().convertXmrToWow(amount);
                } catch (NumberFormatException | NullPointerException amountWrongOrMissingException) {
                    description = XMR_TO_WOW_MISSING_AMOUNT_REPLY;
                }
            }
            default -> {
                embedTitle = "Conversion error :(";
                description = "Oops, sorry Anon, I encountered an error during the conversion :(";
            }
        }
        return createMessageEmbed(embedTitle, COINGECKO_LINK, description, null, WOWNERO_PINK);
    }

    public MessageEmbed getWowReply() {
        String embedTitle = "How to get some WOW";
        String embedTitleUrl = "https://tradeogre.com/exchange/BTC-WOW";
        String description = "Buy and sell WOW on [TradeOgre](https://tradeogre.com/exchange/BTC-WOW) ;)";

        return createMessageEmbed(embedTitle, embedTitleUrl, description, null, WOWNERO_PINK);
    }
}